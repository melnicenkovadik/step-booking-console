package entities;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest {
    private int id = 120;
    private String flightNumber = "KX-120";
    private String from = "Киев";
    private String to = "Харьков";
    private String takeoffTime = "05/08/2019 21-45";
    private String landingTime = "05/08/2019 23-45";
    private int emptySeats = 9;
    private Flight flight = new Flight(id, flightNumber, from, to, takeoffTime, landingTime, emptySeats);

    @Test
    void getId() {
        assertEquals(id, flight.getId());
    }

    @Test
    void getFlightNumber() {
        assertEquals(flightNumber, flight.getFlightNumber());
    }

    @Test
    void getFrom() {
        assertEquals(from, flight.getFrom());
    }

    @Test
    void getTo() {
        assertEquals(to, flight.getTo());
    }

    @Test
    void getTakeoffTime() {
        assertEquals(takeoffTime, new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(flight.getTakeoffTime())));
    }

    @Test
    void getLandingTime() {
        assertEquals(landingTime, new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(flight.getLandingTime())));
    }

    @Test
    void getEmptySeats() {
        assertEquals(emptySeats, flight.getEmptySeats());
    }

    @Test
    void setEmptySeats() {
        assertTrue(flight.setEmptySeats(10));
    }

    @Test
    void to_string() {
        String flight_string = "Рейс:[" +
                "ID:" + id +
                "; Номер рейса:" + flightNumber +
                "; Из:" + from +
                "; В:" + to +
                "; Время взлета:" + takeoffTime +
                "; Время посадки:" + landingTime +
                "; Свободных мест:" + emptySeats +
                ']';

        assertEquals(flight_string, flight.toString());
    }
}