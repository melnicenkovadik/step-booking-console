package entities;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    private int flightId = 121;
    private ArrayList<Integer> passengers = new ArrayList<>(Arrays.asList(1, 2, 3));
    private Booking booking = new Booking(flightId, passengers);

    @Test
    void getId() {
        assertEquals(flightId, booking.getId());
    }

    @Test
    void addPassenger() {
        int passengersId = 4;
        assertTrue(booking.addPassenger(passengersId));
    }

    @Test
    void getPassengers() {
        assertEquals(passengers, booking.getPassengers());
    }

    @Test
    void delPassenger() {
        int passengersId = 2;
        assertTrue(booking.delPassenger(passengersId));
    }

    @Test
    void to_string() {
        String booking_string = "ID рейса:" + flightId +
                "; ID пассажиров:" + passengers;
        assertEquals(booking_string, booking.toString());
    }
}