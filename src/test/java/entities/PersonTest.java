package entities;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    private int id = 1;
    private String login = "Alex";
    private String pwd = "123";
    private String name = "Алекс";
    private String surname = "Лазарев";
    private ArrayList<Integer> bookings = new ArrayList<>(Arrays.asList(121, 122, 123));
    Person person = new Person(id, login, pwd, name, surname, bookings);

    @Test
    void getId() {
        assertEquals(id, person.getId());
    }

    @Test
    void getName() {
        assertEquals(name, person.getName());
    }

    @Test
    void getLogin() {
        assertEquals(login, person.getLogin());
    }

    @Test
    void getPwd() {
        assertEquals(pwd, person.getPwd());
    }

    @Test
    void getSurname() {
        assertEquals(surname, person.getSurname());
    }

    @Test
    void getBookings() {
        assertEquals(bookings, person.getBookings());
    }

    @Test
    void addBooking() {
        assertTrue(person.addBooking(127));
    }

    @Test
    void to_string() {
        String person_string = "User:[" +
                "ID:" + id +
                "; Имя:" + name +
                "; Фамилия:" + surname +
                "; ID бронирований:" + bookings.toString() +
                ']';

        assertEquals(person_string, person.toString());
    }
}