package daoimpl;

import dao.DAOFactory;
import dao.Identifiable;
import entities.Booking;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DAOHashMapBooking implements DAOFactory<Identifiable> {
    private HashMap<Integer, Identifiable> map = new HashMap<>();
    private int flightId;
    private ArrayList<Integer> passengers;
    private File file = new File("./data", "booking.txt");
    private Stream<String> lines;

    {
        try {
            lines = new BufferedReader(new FileReader(file)).lines();
            lines.forEach(new Consumer<String>() {
                @Override
                public void accept(String line) {
                    String[] params = line.split("; ");
                    for (String param : params) {
                        String[] item = param.split(":");
                        switch (item[0]) {
                            case "flightID": {
                                flightId = Integer.parseInt(item[1]);
                                break;
                            }
                            case "passengerID": {
                                String[] passengersStr = item[1].split(",");

                                for (int j = 0; j < passengersStr.length; j++) {
                                    passengers.add(Integer.parseInt(passengersStr[j]));
                                }
                                break;
                            }
                        }
                        insert(new Booking(flightId, passengers));
                        passengers = new ArrayList<>();
                    }
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Identifiable get(int id) {
        try{
            return map.get(id);
        } catch (Exception e){
            map.put(id, new Booking(id, new ArrayList<Integer>()));
            return map.get(id);
        }
    }

    @Override
    public List<Identifiable> getAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public boolean insert(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean update(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean remove(Identifiable c) {
        map.remove(c.getId());
        return true;
    }


    public void writeToFile(){
        Path path = Paths.get("./data", "booking.txt");
        ArrayList<String> content = new ArrayList<>();

        for(Map.Entry<Integer, Identifiable> item : map.entrySet()){
            StringBuilder line = new StringBuilder();
            Booking booking = (Booking)item.getValue();
            line.append("flightID:").append(booking.getId()).append("; ")
                    .append("passengerID:");
            if(booking.getPassengers().size() != 0){
                booking.getPassengers().forEach( personsId -> {
                    line.append(personsId).append(",");
                });
                content.add(line.toString());
                try {
                    Files.write(path, content);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}