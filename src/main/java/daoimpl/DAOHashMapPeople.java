package daoimpl;

import dao.DAOFactory;
import dao.Identifiable;
import entities.Person;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DAOHashMapPeople implements DAOFactory<Identifiable> {
    private HashMap<Integer, Identifiable> map = new HashMap<>();
    private int id;
    private String login;
    private String pwd;
    private String name;
    private String surname;
    private ArrayList<Integer> bookings = new ArrayList<>();

    private File file = new File("./data", "users.txt");
    private Stream<String> lines;
    {
        try {
            lines = new BufferedReader(new FileReader(file)).lines();
            lines.forEach(new Consumer<String>() {
                @Override
                public void accept(String line) {
                    String[] params = line.split("; ");
                    for (String param : params) {
                        String[] item = param.split(":");
                        switch (item[0]) {
                            case "id": {
                                id = Integer.parseInt(item[1]);
                                break;
                            }
                            case "login": {
                                login = item[1];
                                break;
                            }
                            case "pwd": {
                                pwd = item[1];
                                break;
                            }
                            case "name": {
                                name = item[1];
                                break;
                            }
                            case "surname": {
                                surname = item[1];
                                break;
                            }
                            case "bookings": {
                                try {
                                    String[] booking = item[1].split(",");
                                    for (String s : booking) {
                                        bookings.add(Integer.parseInt(s));
                                    }
                                    break;
                                } catch (Exception e){
                                    bookings = new ArrayList<Integer>();
                                }

                            }
                        }
                        insert(new Person(id, login, pwd, name, surname, bookings));
                        bookings = new ArrayList<>();
                    }
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Identifiable get(int id) {
        return map.get(id);
    }

    @Override
    public List<Identifiable> getAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public boolean insert(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean update(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean remove(Identifiable c) {
        map.remove(c.getId());
        return true;
    }


    public void writeToFile(){
        Path path = Paths.get("./data", "users.txt");
        ArrayList<String> content = new ArrayList<>();

        for(Map.Entry<Integer, Identifiable> item : map.entrySet()){
            StringBuilder line = new StringBuilder();
            Person person = (Person) item.getValue();
            line.append("id:").append(person.getId()).append("; ")
                    .append("login:").append(person.getLogin()).append("; ")
                    .append("pwd:").append(person.getPwd()).append("; ")
                    .append("name:").append(person.getName()).append("; ")
                    .append("surname:").append(person.getSurname()).append("; ")
                    .append("bookings:");

            List<Integer> bookings = person.getBookings();
            bookings.forEach(bookingId -> {
                line.append(bookingId).append(",");
            });
            content.add(line.toString());
        }
        try {
            Files.write(path, content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
