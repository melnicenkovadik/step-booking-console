package daoimpl;

import dao.DAOFactory;
import dao.Identifiable;
import entities.Flight;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DAOHashMapFlights implements DAOFactory<Identifiable> {
    private HashMap<Integer, Identifiable> map = new HashMap<>();
    private int id;
    private String flightNumber;
    private String from;
    private String to;
    private String takeoffTime;
    private String landingTime;
    private int emptySeats;
    private File file = new File("./data", "flights.txt");
    private Stream<String> lines;

    {
        try {
            lines = new BufferedReader(new FileReader(file)).lines();
            lines.forEach(new Consumer<String>() {
                @Override
                public void accept(String line) {
                    String[] params = line.split("; ");
                    for (String param : params) {
                        String[] item = param.split(":");
                        switch (item[0]) {
                            case "id": {
                                id = Integer.parseInt(item[1]);
                                break;
                            }
                            case "flightNumber": {
                                flightNumber = item[1];
                                break;
                            }
                            case "from": {
                                from = item[1];
                                break;
                            }
                            case "to": {
                                to = item[1];
                                break;
                            }
                            case "takeoffTime": {
                                takeoffTime = item[1];
                                break;
                            }
                            case "landingTime": {
                                landingTime = item[1];
                                break;
                            }
                            case "emptySeats": {
                                emptySeats = Integer.parseInt(item[1]);
                                break;
                            }
                        }
                    }
                    insert(new Flight(id, flightNumber, from, to, takeoffTime, landingTime, emptySeats));
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Identifiable get(int id) {
        return map.get(id);
    }

    @Override
    public List<Identifiable> getAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public boolean insert(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean update(Identifiable c) {
        map.put(c.getId(), c);
        return true;
    }

    @Override
    public boolean remove(Identifiable c) {
        map.remove(c.getId());
        return true;
    }

    public void writeToFile(){
        Path path = Paths.get("./data", "flights.txt");
        ArrayList<String> content = new ArrayList<>();

        for(Map.Entry<Integer, Identifiable> item : map.entrySet()){
            StringBuilder line = new StringBuilder();
            Flight flight = (Flight) item.getValue();
            line.append("id:").append(flight.getId()).append("; ")
                    .append("flightNumber:").append(flight.getFlightNumber()).append("; ")
                    .append("from:").append(flight.getFrom()).append("; ")
                    .append("to:").append(flight.getTo()).append("; ")
                    .append("takeoffTime:").append(new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(flight.getTakeoffTime()))).append("; ")
                    .append("landingTime:").append(new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(flight.getLandingTime()))).append("; ")
                    .append("emptySeats:").append(flight.getEmptySeats());
            content.add(line.toString());
        }
        try {
            Files.write(path, content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
