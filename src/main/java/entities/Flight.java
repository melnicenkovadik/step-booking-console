package entities;

import dao.Identifiable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Flight implements Identifiable {
    private final int id;
    private final String flightNumber;
    private final String from;
    private final String to;
    private final long takeoffTime;
    private final long landingTime;
    private int emptySeats;

    public Flight(int id, String flightNumber, String from, String to, String takeoffTime, String landingTime, int emptySeats) {
        this.id = id;
        this.flightNumber = flightNumber;
        this.from = from;
        this.to = to;
        this.takeoffTime = getMilliseconds(takeoffTime);
        this.landingTime = getMilliseconds(landingTime);
        this.emptySeats = emptySeats;
    }

    private long getMilliseconds(String flightTime){
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH-mm", Locale.ENGLISH);
        Date date;

        try {
            date = format.parse(flightTime);
            return date.getTime();
        } catch (ParseException e) {
            System.out.println("Неверный формат даты: " + flightTime);
            return 0;
        }
    }

    @Override
    public int getId() {
        return id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public long getTakeoffTime() {
        return takeoffTime;
    }

    public long getLandingTime() {
        return landingTime;
    }

    public int getEmptySeats() {
        return emptySeats;
    }

    public boolean setEmptySeats(int emptySeats) {
        this.emptySeats = emptySeats;
        return true;
    }

    @Override
    public String toString() {
        return "Рейс:[" +
                "ID:" + id +
                "; Номер рейса:" + flightNumber +
                "; Из:" + from +
                "; В:" + to +
                "; Время взлета:" + new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(takeoffTime)) +
                "; Время посадки:" + new SimpleDateFormat("dd/MM/yyyy HH-mm").format(new Date(landingTime)) +
                "; Свободных мест:" + emptySeats +
                ']';
    }
}
