package entities;

import dao.Identifiable;

import java.util.ArrayList;

public class Person implements Identifiable {
    private final int id;
    private final String login;
    private final String pwd;
    private final String name;
    private final String surname;
    private final ArrayList<Integer> bookings;

    public Person(int id, String login, String pwd, String name, String surname, ArrayList<Integer> bookings) {
        this.id = id;
        this.login = login;
        this.pwd = pwd;
        this.name = name;
        this.surname = surname;
        this.bookings = bookings;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPwd() {
        return pwd;
    }

    public String getSurname() {
        return surname;
    }

    public ArrayList<Integer> getBookings() {
        return bookings;
    }

    public boolean addBooking(int id) {
        bookings.add(id);
        return true;
    }

    @Override
    public String toString() {
        return "User:[" +
                "ID:" + id +
                "; Имя:" + name +
                "; Фамилия:" + surname +
                "; ID бронирований:" + bookings +
                ']';
    }
}
