package entities;

import dao.Identifiable;

import java.util.ArrayList;

public class Booking implements Identifiable {
    private final int flightId;
    private final ArrayList<Integer> passengers;

    public Booking(int flightId, ArrayList<Integer> passengers) {
        this.flightId = flightId;
        this.passengers = passengers;
    }

    @Override
    public int getId() {
        return flightId;
    }

    public boolean addPassenger(int id) {
        passengers.add(id);
        return true;
    }

    public boolean delPassenger(int id) {
        for(int i = 0; i < passengers.size(); i++){
            if(passengers.get(i) == id){
                passengers.remove(i);
                return true;
            }
        }
        return false;
    }

    public ArrayList<Integer> getPassengers() {
        return passengers;
    }

    @Override
    public String toString() {
        return "ID рейса:" + flightId +
                "; ID пассажиров:" + passengers;
    }
}
